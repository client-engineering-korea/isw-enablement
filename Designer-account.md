# Designer Account

## Designer URL

[Designer](https://edu-k5-designer.apps.openshift-01.knowis.cloud/)

[Envoy](https://education-dev.apps.openshift-01.knowis.cloud/)

---

| gitlab계정    | ID     | Password |
| ------------- | ------ | -------- |
| hmw8572       | user2  | user2    |
| moondh25      | user3  | user3    |
| choi_2630     | user4  | user4    |
| kwanhoon9     | user5  | user5    |
| yuncheol_keum | user6  | user6    |
| kuku8805note  | user7  | user7    |
| dntndus9611   | user8  | user8    |
| seonmin1219   | user9  | user9    |
| isw8052423    | user10 | user10   |
| gyupyo        | user11 | user11   |
| sksong921     | user12 | user12   |
| creatorhong   | user13 | user13   |
| hs940920      | user14 | user14   |
| jwpark.db     | user15 | user15   |
| hyshin01      | user16 | user16   |
| tapple135     | user17 | user17   |
| DaeHyunYang   | user18 | user18   |
| Minhee.kim    | user19 | user19   |
| aplusshkim    | user20 | user20   |
| s-jeongeun    | user21 | user21   |
| hyukseoung    | user22 | user22   |
| kanghk        | user23 | user23   |
| flehddus87    | user24 | user24   |
| libertyfree   | user25 | user25   |
| ??????????    | user26 | user26   |
| opopqkr1      | user27 | user27   |
| yjyoo9112     | user28 | user28   |

