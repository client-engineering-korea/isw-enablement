# API Design

![API Namespace](images/servicing-order-api.png){width="50%"}

## - API Namespace 설정

![Domain Namespace](images/api-namespace.png){width="50%"}

> | 이름                                    | 설명                                     |
> | --------------------------------------- | ---------------------------------------- |
> | Prefix                                  | Unique한 API Path명 주로 API 버전을 명시 |
> | Label                                   | API Label                                |
> | from scratch                            | 처음부터 API 설계                        |
> | based on uploaded OpenAPI / Swaggerfile | OpenAPI스펙에 따른 파일로 시작           |

---

## - Paths 생성

> | 이름 | 값           |
> | ---- | ------------ |
> | Path | /withddrawal |

---

## - Schemas 생성

> ### CustomerReference
>
> | 이름     | 값     |
> | -------- | ------ |
> | id       | String |
> | password | String |

> ### withdrawalBodySchema
>
> | 이름               | 값     |
> | ------------------ | ------ |
> | Customer Reference | Object |
> | Account Number     | String |
> | Amount             | String |

> ### withdrawalResponseSchema
>
> | 이름                         | 값                        |
> | ---------------------------- | ------------------------ |
> | servicingOrderWorkTaskResult | String                   |
> | Transaction ID               | String                   |

> ### ErrorSchema
>
> | 이름     | 값     |
> | -------- | ------ |
> | ErrorId  | Stirng |
> | ErrorMsg | String |

---

## - Request Body 생성

> ### WithdrawalRequestBody
>
> | 이름             | 값                    |
> | ---------------- | --------------------- |
> | Local Identifier | WithdrawalRequestBody |
> | Required         | true                  |
> | Content-Type     | application/json      |
> | Schema           | withdrawalBodySchema  |

---

## - Response 생성

> ### WithdrawalResponse
>
> | 이름             | 값                       |
> | ---------------- | ------------------------ |
> | Local Identifier | WithdrawalResponse       |
> | Content-Type     | application/json         |
> | Schema           | withdrawalResponseSchema |

> ### ErrorResponse
>
> | 이름             | 값               |
> | ---------------- | ---------------- |
> | Local Identifier | ErrorResponse    |
> | Content-Type     | application/json |
> | Schema           | ErrorSchema      |

---

## - Operation 생성

> ### POST /withdrawal
>
> | 이름         | 값                           |
> | ------------ | ---------------------------- |
> | Operation Id | ServicingOrderWithdrawalPost |

---

### Request Body 추가

> Add referenced request body -> WithdrawRequestBody 선택

### Response 추가

> Add referenced response
> | Status Code | Local Identifier |
> | ------------ | ---------------------------- |
> | 201 | WithdrawlResponse |
> | 500 | ErrorResponse |
