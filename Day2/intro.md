# Solution Design

## ISW Solution Layer

![isw-layer](images/isw_layer.png)

## Domain

### Domain Design

#### - [용어 설명](domain.md)

#### - [Domain Design](domain-design.md)

---

## API

### API Design

#### - [용어 설명](api.md)

#### - [API Design](api-design.md)

---

## Integration

### Integration Design

#### - [Integration Design](integration-design.md)
